### Chapter 2. Infrastructure Device Access

> Tài liệu: Network Security Baseline

> Thực hiện: Nguyễn Công Trứ

> Cập nhật: 19/05/2017

### Mục lục

- [Truy cập thiết bị cơ hở hạ tầng](#1)

	+ [Total Visibility](#1.1)

	+ [Complete Control](#1.2)

- [Hạn chế quyền quản lý các thiết bị cơ sở hạ tầng](#2)

	+ [Tương tác với các thiết bị cisco IOS và quản lý các dòng truy cập](#2.1)

		- [AUX Port](#2.1.1)

		- [Console Port](#2.1.2)

		- [VTY Line](#2.1.3)

	+ [Vô hiệu hóa các thiết bị không cần thiết và quản lý các port truy cập](#2.2)

	+ [Hạn chế truy cập thiết bị đến các dịch vụ ủy quyền và các giao thức](#2.3)
	

	+ [Hạn chế các nỗ lực truy cập vào các thiết bị đối với các dịch vụ được ủy quyền bởi người quản trị](#2.4)

		- [Standard ACLs](#2.4.1)

		- [Extended ACLs](#2.4.2)
	
	+ [Thực thi xác thực đăng nhập sử dụng AAA](#2.5)
	
	+ [Thực thi ủy quyền đăng nhập các thiết bị sử dụng AAA](#2.6)

	+ [Thực thi xác thực độ ưu tiên sử dụng AAA](#2.7)

	+ [Thực thi quản lí các session - phiên](#2.8)

		- [Idle Sessions](#2.8.1)

		- [Hung Sessions](#2.8.2)

	+ [Hạn chế lỗ hổng đăng nhập từ tấn công bằng từ điển và DoS ](#2.9)

		- [Sử dụng mật khẩu mạnh](#2.9.1)

		- [Tính năng độ dài mật khẩu tối thiểu trong thiết bị Cisco IOS](#2.9.2)

		- [Hạn chế số lần cố gắn đăng nhập](#2.9.3)

		- [Hạn chế số lần đăng nhập không thành công trong thời gian nhất định](#2.9.4)

		- [Giới hạn terminal và  port quản lý](#2.9.5)

	+ [Bảng thông báo pháp lý](#2.10)

- [AAA Services](#3)

	+ [AAA Overview](#3.1)

	+ [AAA Server Groups](#3.2)

	+ [AAA Method Lists](#3.3)

	+ [AAA Server Communication Security](#3.4)

	+ [AAA Server Based Accounting Services](#3.5)

- [Secure Shell (SSH)](#4)

- [Web-based GUI Access](#5)

	+ [HTTP](#5.1)

	+ [HTTPS](#5.2)

- [SNMP Access](#6)

- [Bảo vệ thông tin lưu trữ cục bộ](#7)

	+ [Mã hõa mật khẩu toàn cục](#7.1)

	+ [Mã hóa mật khẩu người dùng cục bộ](#7.2)

	+ [Kích hoạt secret](#7.3)

- [Chấp nhận truy cập quản lý thiết bị cơ sở hạ tầng ](#8)

	+ [AAA EXEC Accounting](#8.1)

	+ [AAA Failed Authentication Accounting](#8.2)

	+ [AAA Command Accounting](#8.3)

	+ [AAA System Accounting](#8.4)

	+ [Syslog Login Success and Failure Notifications](#8.5)

	+ [Configuration Change Notification and Logging](#8.6)

	+ [Displaying Configuration Change Log Entries](#8.7)

	+ [General Device Access and Configuration Change Logging Best Common Practices](#8.8)

- [File Transfer](#9)

	+ [File Transfer Protocol (FTP)](#9.1)

	+ [Trivial File Transfer Program (TFTP)](#9.2)

	+ [Secure Copy (SCP)](#9.3)

- [Device Software Image Verification](#10)

	+ [IOS Software Image Verification](#10.1)

- [Infrastructure Management Network](#11)

- [Device Management Best Common Practices](#12)

***

<a name='1'></a>
## Truy cập thiết bị cơ hở hạ tầng

Tự bảo vệ cơ sở hạ tầng mạng là rất quan trọng đối với toàn bộ cơ sở an ninh mạng, có thể là các router, switch, server và các thiết bị hạ tầng khác. Một yếu tố quan trọng trong việc này là quản lý truy cập đến các thiết bị cơ sở hạ tầng. Nếu các thiết bị hạ tầng này bị ảnh hưởng thì việc bảo mật và quản lý của toàn mạng sẽ bị tổn hại. Do đó, điều quan trọng là phải thiết lập các biện pháp kiểm soát thích hợp để ngăn chặn việc truy cập bất hợp pháp đến các thiết bị hạ tầng.

Các thiết bị cơ sở hạ tầng thường cung cấp 1 loạt các cơ chế truy cập khác nhau, bao gồm giao diện diều khiển (console) và kết nối đồng bộ, cũng như truy cập từ xa dựa trên các giao thức như Telnet, rlogin, HTTP và SSH. Một vài cơ chế được bật mặc định với độ ưu tiên tối thiếu đối với chúng. Ví dụ, Các nền tảng tương tự như phần mềm Cisco IOS sẽ chuyển sang chế độ giao diện điều khiển truy cập 1 cách mặc định. Vì lý do đó, mỗi thiết bị cơ sở hạ tầng nên được xem xét kiểm tra và cấu hình cẩn thận đảm bảo chỉ bật các cơ chế truy cập được hỗ trợ và chúng đảm bảo cấu hình đúng.

Các bước chính để đảm bảo cho cả tương tác và quản lý truy cập đến các thiết bị cơ sở hạ tầng là:

- Hạn chế khả năng tiếp cận thiết bị: Giới hạn các cổng được truy cập, hạn chế cho phép truyền thông và các phương pháp truy cập.

- Trình bày thông báo pháp lý: Hiển thị thông báo pháp lý, được phát triển cùng với luật của công ty, cho các phiên tương tác.

- Xác thực quyền truy cập: Đảm bảo truy cập chỉ được cấp cho các user, group và các dịch vụ được xác thực.

- Ủy quyền hành động: Hạn chế các hành động và chế độ xem được cho phép bởi bất user, group hoặc dịch vụ nào.

- Đảm bảo tính bảo mật của dữ liệu: Bảo vệ dữ liệu dạy cảm được lưu trữ cục bộ từ việc xem và sao chép dữ liệu. Khảo sát khả năng dễ bị tấn công của dữ liệu khi trao đổi trên đường truyền để nghe lắng, tấn công phiên làm việc và man-in-the-middle (MITM).

- Nhật ký và tài khoản cho tất cả các truy cập: Ghi lại những người đã truy cập vào thiết bị, đã xảy ra điều gì và khi nào cho mục đích kiểm tra.

Note: Điều quan trọng là phải thường xuyên xem lại các bản ghi để kiểm tra truy cập và xác định bất kỳ truy cập hoặc hành động trái phép nào.

#### Đánh giá phương pháp CSF

Kết quả của việc áp dụng CSF đối với cơ sở an ninh mạng truy cập các thiết bị cơ sở hạ tầng được trình bày trong 2 bảng dưới. Các bảng làm nổi bật các công nghệ và tính năng được xác định cho truy cập quản lý truy cập thiết bị và được tích hợp trong cơ sở an ninh mạng.

<a name='1.1'></a>
### Total Visibility

<p align='center'> <img src="https://gitlab.com/hellsins/Side_Project/raw/master/Network_Security_Baseline/Image/2-1.png"/></p>

<a name='1.2'></a>
### Complete Control

<p align='center'> <img src="https://gitlab.com/hellsins/Side_Project/raw/master/Network_Security_Baseline/Image/2-2.png"/></p>

<a name='2'></a>
## Hạn chế quyền quản lý các thiết bị cơ sở hạ tầng

Bước đầu tiên đảm bảo quản lý truy cập đến các thiết bị cơ sở hạ tầng là hạn chế truy cập đến các thiết bị. Những yếu tố quan trọng bao gồm:

- Hạn chế truy cập được ủy quyền cho thiết bị đầu cuối và các port quản lý 

- Hạn chế truy cập được ủy quyền cho các dịch vụ và giao thức

- Hạn chế các nỗ lực truy cập vào các dịch vụ được ủy quyền bởi các nhà sản xuất

- Chỉ cấp quyền cho người dùng được xác thực và ủy quyền

- Cấp quyền ưu tiên tối thiểu cho người dùng được ủy quyền

- Thực hiện quản lý các phiên

- Hạn chế các lỗ hổng từ việc tấn công từ điển và DoS

Cách tiếp cận chung để đạt được các mục tiêu này được thể hiện trong bảng dưới. Triết lý chung là quyền truy cập quản lý các thiết bị phải được deny, và chỉ cho phép đối với những người dùng và dịch vụ được yêu cầu rõ ràng.

|Hạn chế truy cập quản lý cá thiết bị hạ tầng|Phương pháp chung|
|--|--|
| - Hạn chế truy cập được ủy quyền cho thiết bị đầu cuối và các port quản lý| + Vô hiệu hóa tất cả các thiết bị đầu cuối và quản lý các port mà không được yêu cầu rõ ràng hoặc chủ động được sử dụng cho việc quản lý truy cập các thiết bị|
| - Hạn chế truy cập được ủy quyền cho các dịch vụ và giao thức| + Cho phép quản lý các thiết bị truy cập thông qua các dịch vụ và giao thức được yêu cầy và được hỗ trợ|
|| + Deny tất cả các dịch vụ và giao thức truy cập quản lý các thiết bị khác|
|| + Deny truy cập ra ngoài trừ khi được yêu cầu rõ ràng|
| - Hạn chế các nỗ lực truy cập vào các dịch vụ được ủy quyền bởi các nhà sản xuất| + Chỉ cho phép truy cập vào các thiết bị được ủy quyền từ những người khởi tạo có đủ quyền|
| - Chỉ cấp quyền cho người dùng được xác thực và ủy quyền| + Sử dụng AAA để xác thực và ủy quyền quản lý truy cập các thiết bị trên tất cả các port và dịch vụ được hỗ trợ|
| - Thực hiện quản lý các phiên| + Thực hiện thời gian chờ và không hoạt động và các phương pháp lưu giữ để phát hiện và đóng các session không hoạt động hoặc treo|
|| + Thực hiện chủ động chờ timeout để hạn chế thời gian tối đa của phiên trước khi xác thực lại|
| - Hạn chế các lỗ hổng từ việc tấn công từ điển và DoS| + Hạn chế tốc độ đăng nhập|
|| + Thực hiện khóa trong 1 khoản thời gian nếu xác thực thất bại nhiều lần|
|| + Dự phòng 1 cổng quản lý cho truy cập chỉ bởi 1 máy chủ NoC cụ thể|

Note: Hầu hết các thiết bị cơ sở hạ tầng có thể được truy cập thông qua nhiều dạng thiết bị đầu cuối và các port quản lý, các dịch vụ và giao thức, một số có thể được bật mặc định. Tất cả các cơ chế quản lý truy cập có thể được xem xét và bảo đảm.

<a name='2.1'></a>
### Tương tác với các thiết bị cisco IOS và quản lý các dòng truy cập

Các nền tảng dựa trên phần mềm Cisco IOS thường cung cấp quyền truy cập quản lý các thiết bị tương tác thông qua các port và các line:

- TTY lines: Các port không đồng bộ, bao gồm:

	+ AUX

	+ console

- VTY lines: Các line VTY ảo được sử dụng để truy cập từ xa như:

	+ Telnet

	+ SSH
	
	+ rlogin

Lưu ý các truy cập với giao diện web-based (HTTP/HTTPS) và SNMP sẽ được trình bày trong các phần tiếp theo.

<a name='2.1.1'></a>
#### AUX Port

Tương tác truy cập qua port AUX thường được sử dụng để cung cấp truy cập quản lý dial-in hoặc dial-out đến 1 nền tảng. Nếu điều này không được yêu cầu, line (đường kết nối) này cần phải bị vô hiệu hóa để giảm nguy cơ truy cập trái phép.

<a name='2.1.2'></a>
#### Console Port

Tương tác truy cập qua cổng console được truy cập trực tiếp từ người dùng cục bộ hoặc truy cập từ xa qua việc sử dụng terminal hợc máy chủ console. Nếu truy cập cổng console được yêu cầu, line (đường kết nối) cần đảm bảo đúng để ngăn chặn truy cập trái phép.

Note: Nếu sử dụng máy chủ đầu cuối, hãy chắc chắn đảm bảo rằng các thiết bị được bảo vệ bằng cách thực thi các nguyên tắc bảo mật được trình bày trong tài liệu này.

<a name='2.1.3'></a>
#### VTY Line

Tương tác truy cập thông qua VTY là phương thức được sử dụng phổ biến nhất để truy cập từ xa 1 thiết bị. Nếu truy cập VTY được yêu cầu, line (đường kết nối) cần được đảm bảo đúng để ngăn chặn truy cập trái phép. 

Một vài cấu hình mẫu về TTY và VTY được cung cấp ở các phần sau.

Note: Một router thường có 5 đường VTY (từ VTY 0 đến 4) nhưng có thể hỗ trợ nhiều hơn. Điều quan trọng là đảm bảo bảo mật cho các đường VTY sẵn có.

<a name='2.2'></a>
### Vô hiệu hóa các thiết bị không cần thiết và các port truy cập quản lý

Một số thiết bị cơ sở hạ tầng mạng có cổng terminal và cổng quản lý được enable mặc định. Điều này có thể gây nguy cơ bảo mật. Nó được khuyến cáo cần vô hiệu hóa tất cả các cổng terminal và cổng quản lý và các interface mà không được yêu cầu hoặc không được sử dụng.

Trên các thiết bị Cisco IOS, các cổng terminal và cổng quản lý bao gồm các đường kết nối TTY và VTY. Các port này có thể được vô hiệu hóa,  sử dụng lệnh `no exec` để show cấu hình và kết quả như sau:

```
!
! Disable access to VTY
line vty 1
login
no exec
!
! Disable access to Console
line con 0
no exec
```

<a name='2.3'></a>
### Hạn chế truy cập thiết bị đến các dịch vụ ủy quyền và các giao thức

Một số thiết cơ sở hạ tầng mạng có dịch vụ truy cập quản lý thiết bị và các giao thức được mở mặc định. Điều này có thể gây ra nguy cơ bảo bật. Nó được khuyến cáo cần vô hiệu hóa tất cả các dịch vụ và giao thức truy cập quản lý các thiết bị mà không được yêu cầu hoặc không được sử dụng.

Trên các thiết bị Cisco IOS, các dịch vụ và giao thức truy cập quản lý thiết bị thường bao gồm:

- Truy cập thông qua Telnet, SSH,...

- HTTP, HTTPS

- SNMP

Tương tác truy cập, thông qua các line TTY và VTY của các thiết bị Cisco IOS, nên hạn chế chỉ cho phép các dịch vụ và giao thức truy cập được phép, theo chính sách bảo mật của công ty. Các hạn chế cần thực hiện với cả kết nối vào và ra.

Điều này được thực thi trên các line TTY và VTY sử dụng lệnh `transport`. Một vài ví dụ được cung cấp trong bảng sau:

|Các line Cisco IOS TTY và VTY. Hình thức hạn chế truy cập quản lý các thiết bị|Cấu hình IOS trên TTY và VTY|
|--|--|
|Không kết nối vào|`transport input none`|
|Không kết nối ra|`transport output none`|
|Chỉ cho phép SSH với kết nối vào|`transport input ssh`|
|Chỉ cho phép Telnet với kết nối vào|`transport input telnet`|
|Cho phép SSH hoặc Telnet với kết nối vào|`transport input telnet ssh`|
|Chỉ cho phép SSH cho kết nối ra ngoài|`transport output ssh`|
|Truyền tải giao thức được xác định trong yêu cầu kết nối|`transport preferred none`|

Note: Thực tế tốt nhất là các giao thức truy cập mã hóa, như SSH, tốt hơn các giao thức dạng text như Telnet.

SSH được đề cập chi tiết hơn trong phần `Secure Shell (SSH)`

Các hướng dẫn bảo mật với HTTP, HTTPS và SNMP được mô tả trong các phần sau

Để biết nhiều thông tin hơn về lệnh cấu hình `transport`, tìm hiểu tại http://www.cisco.com/en/US/docs/ios/12_3/termserv/command/reference/ter_t1g.html#wp1083564

<a name='2.4'></a>
### Hạn chế các nỗ lực truy cập vào các thiết bị đối với các dịch vụ được ủy quyền bởi người quản trị

Chỉ những người ủy quyền cấu hình, nên cho phép thậm chí thử quyền truy cập quản lý các thiết bị, và chỉ các dịch vụ mà họ được ủy quyền sử dụng. Điều này đảm bảo rằng việc sử lý các yêu cầu truy cập được hạn chế đối với các dịch vụ được phân quyền cho 1 địa chỉ IP nguồn. Điều này làm giảm nguy cơ truy cập trái phép và tránh các cuộc tấn công như tấn công quét cạn (brute force), từ điển hay tấn công từ chối dịch vụ (DoS)

Trên các thiết bị Cisco IOS, `standard ACL` có thể được sử dụng để hạn chế sự nỗ lực truy cập quản lý các thiết bị đối với người quản trị. `Extended ACL` có thể được sử dụng cho sự nỗ lực truy cập quản lý các thiết bị tới các dịch vụ được phân quyền bởi người quản trị.

Cần lưu ý rằng càng thêm hạn chế trong ACL thì càng giới hạn được việc nỗ lực truy cập trái phép. Tuy nhiên, càng hạn chế nhiều, như hạn chế quyền truy cập vào máy chủ Noc, có thể tạo chi phí quản lý, có thể ảnh hưởng đến truy cập nếu mất kết nối mạng trên 1 vị trí cụ thể và có thể giới hạn khả năng của quyền quản trị khi thực hiện trong quá trình khắc phục sự cố, ví dụ như những người ở xa remote xem xét các sự cố trên mạng cục bộ. Do đó, có sự cân bằng được xem xét. Một thỏa hiệp là hạn chế các truy cập vào IP nội bộ của công ty.

Note: Mỗi khách hàng cần đánh giá việc thực hiện ACLs liên quan đến chính sách an ninh, rủi ro, tiếp xúc và chấp nhận của chính mình.

<a name='2.4.1'></a>
#### Standard ACLs

`Standard ACLs` cho phép hạn chế được thực thi trên 1 IP nguồn hoặc 1 dãy IP.

```
!
access-list 10 permit <NOCsubnet> <inverse-mask>
access-list 10 deny any any
!
```

<a name='2.4.2'></a>
#### Extended ACLs

`Extended ACLs` cho phép hạn chế được thực thi trên IP nguồn hoặc dãy IP và giao thức truy cập.

```
!
access-list <xACL#> permit tcp <NOCsubnet1> <inverse-mask> any eq <TCP port>
access-list <xACL#> permit tcp <NOCsubnet2> <inverse-mask> any eq <TCP port>
access-list <xACL#> deny ip any any log-input
!
```
Note: `Access-class ACLs`chỉ hỗ trợ phần bất kỳ như đích (destination).

ACL sau đó cần được cấu hình (áp vào) trên các line, service và các interface phù hợp để nó được thực thi đúng. Ví dụ về 1 ACL trên VTY như sau

```
!
line vty 0 4
access-class <ACL#> in
!
```

Note: Một ACL cấu hình tốt cũng có thể áp dụng vào VTY để cố gắn bảo vệ việc tương tác truy cập trong 1 cuộc tấn công DoS trên các đường VTY. 

Việc thực thi cho việc truy cập trên SNMP và HTTP được giới thiệu trong các phần sau.

<a name='2.5'></a>	
### Thực thi xác thực đăng nhập sử dụng AAA

Truy cập vào tất cả các port quản lý của thiết bị cơ sở hạ tầng nên được xác thực để hạn chế truy cập mà chỉ cho người dùng có quyền truy cập. Khuyến nghị rằng nên sử  dụng máy chủ AAA tập trung được phát triển để thực thi xác thực đăng nhập trên AAA cho mỗi ngưòi dùng trên tất cả các cổng đầu cuối và port quản lý của thiết bị cơ sở hạ tầng 

Trong Cisco IOS, quyền truy cập quản trị đến thiết bị cơ sở hạ tầng mạng được gọi là `EXEC session` và được thực hiện qua các đường TTY và VTY. Xác thực trên AAA của người dùng đăng nhập EXEC được thực hiện bằng cách áp dụng 1 danh sách các phương thức AAA cho tất cả các đường TTY và VTY có sẵn.

Ví dụ cấu hình IOS cho việc thực thi xã thực dựa trên AAA-based, với dự phòng cục bộ, người dùng đăng nhập EXEC trên console và VTY line như sau:

```
!
aaa authentication login adminAuthen-list group adminAAAgroup local-case
!
line con 0
login authentication adminAuthen-list
!
line vty 0 4
login authentication adminAuthen-list
!
```

Để biết nhiều hơn về lệnh xác thực đăng nhập trên AAA, có thể xem tại http://www.cisco.com/en/US/docs/ios/12_2/security/configuration/guide/scfathen.html#wp1001032 

<a name='2.6'></a>
### Thực thi ủy quyền đăng nhập các thiết bị sử dụng AAA

Quyền truy cập tối thiểu nên được cấp cho người dùng được xác thực, theo yêu cầu truy cập cụ thể. Điều này làm giảm tiếp xúc với cả sự cố an ninh và sự cố không chủ ý.

Trong Cisco IOS, khả năng kiểm soát người dùng được ủy quyền để mở 1 session EXEC và truy cập vào CLI đạt được bằng cách sử dụng lệnh: `aaa authorization exec`. Kết hợp với AAA, các nhóm người dùng khác nhau có thể dể dàng được cấp quyền truy cập khác nhau.

Một ví dụ của AAA-based EXEC session xác thực truy cập trên kết nói VTY được thực hiện phía dưới, bao gồm dự phòng cho ủy quyền được cấp nếu người dùng được xác thực, trong trường hợp server AAA không có sẵn.

```
!
aaa authorization exec adminAuthor-list group adminAAAgroup if-authenticated
line vty 0 4
authorization exec adminAuthor-list
!
```
Note: Server AAA phải thiết lập `Service-Type` cho EXEC (login) để cấp quyền truy cập session EXEC

Chúng ta có thể tìm hiểu nhiều hơn về lệnh `aaa authorization exec` tại http://www.cisco.com/en/US/docs/ios/12_3/security/command/reference/sec_a1g.html#wp1071720 

<a name="2.7"></a>
### Thực thi xác thực độ ưu tiên sử dụng AAA

Điều quan trọng là đảm bảo người quản trị cố gắn đạt được quyền truy cập đặc biệt được xác thực hợp lệ. Truy cập mức đặc quyền thường đề cập đến khả năng cấu hình của thiết bị cơ sở hạ tầng.

Trong Cisco IOS, quyền truy cập EXEC qua CLI có thể bằng cách sử dụng lệnh `enable` hoặc tự động do kết quả của RADIUS hoặc TACACS+authencation. Sau đó yêu cầu ủy quyền exec và cấu hình độ ưu tiên ở người dùng hoặc nhóm profile trên server AAA.

Cấp đặc quyền truy cập mở rộng cấp độ của session EXEC, cung cấo khả năng cấu hình các thiết bị. Do đố, tùy theo việc thực thi của cấp độ quyền truy cập sẽ, việc cho phép truy cập chỉ được cấp cho cho những người yêu cầi mức truy cập này.

Cisco IOS cho phép truy cập phải được xác thực sử dụng AAA-based cho một máy chủ AAA tập trungvowsi dự phòng cục bộ để enable secret. Điều này hoàn thành bằng cách định nghĩa một danh sách phương thức AAA mặc định cho phép xác thực.

```
!
aaa authentication enable default group adminAAAgroup enable
!
```
Khuyến cáo rằng nên cấu hình enable secret thay vì enable pasword, từ khi enable secret cung cấp kiểu encryption 5 mà nó không thể đảo ngược. Điều này sẽ được nói ở phần sau.

Để biết nhiều hơn về lệnh `aaa authentication enable default`. Chúng ta tìm hiểu tại http://www.cisco.com/en/US/docs/ios/11_3/security/configuration/guide/scauthen.html#wp6292 

<a name='2.8'></a>
### Thực thi quản lí các session - phiên

Các session truy cập thiết bị phải được quản lý để đảm bảo các kịch bản sau đây được giải quyết.

- Idle session

- Hung session

<a name='2.8.1'></a>
#### Idle Sessions

Các Idle sesion không được phép dùng cổng terminal và cổng quản lý vô thời hạn. Điều này bảo tính sẵn sàng của cổng terminal và cổng quản lý, và giảm sự tiếp xúc với việc cướp session (session hijacking).

Trong Cisco IOS, thời gian chờ Idle được cấu hình trên các đường TTY và VTY với lệnh `session-timeout`. Mặc định, session VTY có thời gian chờ 10 phút

```
Router(config-line)# session-timeout <minutes>
```
Lệnh session-timeout này hoạt động khác trên các thiết bị đầu cuối ảo (VTY) trên các bảng điều khiển vật lý, auxiliary (AUX) và terminal (TTY). Khi một timeout xảy ra trên một VTY, session người dùng sẽ trở về dấu nhắc EXEC. Khi thời gian chờ xảy ra trên các đường vật lý, session người dùng đã được đăng xuất và đường trở lại trạng thái không sử dụng.

Bạn có thể sử dụng kết hợp lệnh cấu hình `exec-timeout` và `session-timeout`, thiết lập để gần đúng với các giá trị giống nhau, để có được cùng một hành động từ các đường ảo mà lệnh `session-timeout` gây ra trên đường truyền vật lý.

Trong Cisco IOS, mặc định session VTY có thời gian chờ exec là 10 phút.

```
Router(config-line)# exec-timeout <minutes> [seconds]
```

Để biết nhiều hơn về lệnh `session-timeout` xem tại 

http://www.cisco.com/en/US/docs/ios/12_3/termserv/command/reference/ter_l1g.html#wp1037637 

và lệnh `exec-timeout` xem tại

http://www.cisco.com/en/US/docs/ios/12_3/configfun/command/reference/cfr_1g03.html#wp1029531 

<a name='2.8.2'></a>
#### Hung Sessions

Nếu một hệ thống từ xa bị treo trong khi một  session quản lý đang diễn ra, session vẫn có thể mở và dễ bị tấn công. Do đó, các session hung phải được phát hiện và đóng kết nối để bảo vệ sự sẵn sàng của các cổng terminal và cổng quản lý và giảm khả năng chiếm quyền điều khiển session.

Trong Cisco IOS, session hung trên các đường VTY có thể được phát hiện và đóng kết nối bằng lệnh `service-tcp-keepalives-in`. Điều này làm cho các TCP keepalive được gửi đến các kết nối đến, cho phép phát hiện sự cố của hệ thống từ xa nếu không nhận được phản hồi.

```
Router(config)# service tcp-keepalives-in
```

Để biết nhiều hơn về lệnh `service tcp-keepalives-in` xem tại http://www.cisco.com/en/US/docs/ios/12_3/configfun/command/reference/cfr_1g07.html#wp1029289 

<a name='2.9'></a>
### Hạn chế lỗ hổng đăng nhập từ tấn công bằng từ điển và DoS

Việc dễ bị tấn công khi truy cập quản lý các thiết bị cơ sở hạ tầng mạng từ tấn công bằng từ điển và Dos có thể được giảm bằng cách thực thi các hạn chế sau:

- Sử dụng mật khẩu mạnh

- Hạn chế số lần cố gắn đăng nhập

- Hạn chế số lần đăng nhập thất bại trong thời gian nhất định

- Giới hạn terminal và quản lý port

Các tính năng sẵn có được thực thi các yêu cầu được liệt kê trong bảng sau

|Yêu cầu|Tùy chọn thực thi|
|---|---|
|- Sử dụng mật khẩu mạnh|+ Tính năng máy chủ AAA: Thực hiện việc sử dụng mật khẩu mạnh trên máy chủ AAA, tuân thủ chính sách bảo mật|
||+ Tính năng chiều dài mật khẩu tối thiểu `security passwords min-length`|
|- Hạn chế số lần cố gắn đăng nhập|+ Tính năng tằng cường đăng nhập của Cisco IOS `login delay`|
|- Hạn chế số lần đăng nhập thất bại trong thời gian nhất định|+ Tính năng của máy chủ AAA: Thực thi khóa tài khoản trên máy chủ AAA nếu vượt quá số lần đăng nhập thất bại trong một khoảng thời gian xác định|
||+ Tăng cường đăng nhập Cisco IOS. `login block-for`, `login quiet-mode access-class`|
|- Giới hạn terminal và quản lý port|+ Tính năng của Cisco IOS: Hạn chế ACL cao trên dòng VTY cuối cùng|
|- Ghi log và giám xác người dùng đăng nhập thất bại|+ Sẽ được nói ở phần Infrastructure Device Management Access Logging|

Để biết thêm về việc cải thiệt login của Cisco IOS xem tại http://www.cisco.com/en/US/docs/ios/security/configuration/guide/sec_login_enhance_ps6350_TSD_Products_Configuration_Guide_Chapter.html

<a name='2.9.1'></a>
#### Sử dụng mật khẩu mạnh

Trong trường hợp một cuộc tấn công từ điển, việc sử dụng mật khẩu mạnh sẽ làm cho cuộc tấn công ít có khả năng thành công, vì mật khẩu sẽ không nằm trong các từ điển đơn giản. Nếu một server AAA được dùng để xác thực đăng nhập, server AAA thường cung cấp một tính năng để thực thi việc sử dụng mật khẩu mạnh, tuân theo chính sách bảo mật. Nếu một máy chủ AAA không có sẵn để thực thi chính sách mật khẩu mạnh, thì các tính năng của thiết bị cục bộ dùng để làm giảm thiểu khả năng bị tấn công từ điển nên được sử dụng, nếu có.

Nếu tính năng xác định mật khẩu yếu khi bị tấn công từ điển không có sẵn, một tính năng cơ bản có  sẵn là thực thi chiều dài mật khẩu tối thiểu.  Mặc dù loại tính năng này không cung cấp sự bảo vệ trực tiếp chống lại các cuộc tấn công từ điển nhưng nó cung cấp sự bảo vệ chống lại việc sử dụng các mật khẩu được đoán chung hay dễ đoán như `cisco` và `lab` 



<a name='2.9.2'></a>
#### Tính năng độ dài mật khẩu tối thiểu trong thiết bị Cisco IOS

Cisco IOS cung cấp khả năng thực thi mật khẩu tối thiểu cho user passwords, enable passwords, enable secrets, và line paswords. Tính năng này được kích hoạt trong cấu hình global bằng lệnh

```
Router(config)# security passwords min-length length
```

Khi lệnh này được kích hoạt, bất kỳ mật khẩu nào nhỏ hơn số ký tự được chỉ định sẽ không thành công.

Note: Tính năng này không cung cấp bất kỳ sự bảo vệ nào chống lại các cuộc tấn công từ điển.

Để biết nhiều hơn về lệnh `security passwords min-length` xem tại http://www.cisco.com/en/US/docs/ios/12_3/security/command/reference/sec_r1g.html#wp1081544 

<a name='2.9.3'></a>
#### Hạn chế số lần cố gắn đăng nhập

Trong trường hợp tấn công bằng từ điển, độ trễ giữa các lần đăng nhập sẽ làm chậm quá trình tấn công, tăng thời gian yêu cầu cho cuộc tấn công thành công và khoản thời gian sẵn sàng để xác định sự bất thường và giải quyết.

Trong Cisco IOS, độ trễ giữa các lần đăng nhập kế tiếp có thể được sử dụng bằng lệnh trong cấu hình global. Mặc định độ trễ là 1s

```
Router(config)# login delay <seconds>
```

Để biết nhiều hơn về lệnh `login-delay` xem tại http://www.cisco.com/en/US/docs/ios/security/command/reference/sec_k1.html#wp1031384

<a name='2.9.4'></a>
#### Hạn chế số lần đăng nhập thất bại trong thời gian nhất định

Trong trường hợp tấn công bằng từ điển, việc hạn chế số lần truy cập thất bại trong một khoảng thời gian nhất định có thể làm chậm cuộc tấn công, tăng thời gian cần thiết để thành công xác định và giải quyết các vấn đề bất thường.

Nếu một máy chủ AAA được sử dụng để xác thực đăng nhập, máy chủ AAA thường cung cấp một tính năng là khóa tài khoản nếu xác định đăng nhập vượt quá số lần đăng nhập thất bại cho phép trong một khoảng thời gian nhất định. Nếu một máy chủ AAA không được sử dụng, thì tính năng Cisco IOS có thể được sử dụng.

Trong Cisco IOS, định nghĩa số lần đăng nhập thất bại tối đa được phép trong một khoảng thời gian nhất định, sau đó thiết bị IOS sẽ không chấp nhận bất kỳ nỗ lực kết nối bổ sung nào cho một cấu hình `quiet period`, có thể đạt được bằng cách sử dụng lệnh cấu hình global, Lệnh như sau:

```
Router(config)# login block-for seconds attempts tries within seconds
```

Để biết nhiều hone về lệnh `login block-for` xem tại http://www.cisco.com/en/US/docs/ios/security/command/reference/sec_k1.html#wp1031219 

Cisco IOS cũng cung cấp khả năng định nghĩa ACL ngoại lệ cho các hệ thống tin cậy và mạng lưới mà từ đó các kết nối hợp pháp được mong đợi. Trường hợp ngoại lệ này ACL có thể được định nghĩa với lệnh đăng nhập global `quiet-mode access-class `

```
Router (config)# login quiet-mode access-class
```

Để biết nhiều hơn về lệnh `login quiet-mode access-class` xem tại login quiet-mode access-class

Ví dụ sau chỉ ra cách cấu hình router để nhập khoảng thời gian `quiet period` 100 giây nếu 15 lần đăng nhập thất bại vượt quá 100 giây; Tất cả các yêu cầu đăng nhập sẽ bị từ chối trong suốt thời gian `quiet period`, ngoại trừ máy chủ được định nghĩa trong ACL 10.

```
Router(config)# access-list 10 permit host 172.26.150.206
Router(config)# login block-for 100 attempts 15 within 100
Router(config)# login quiet-mode access-class 10
```

<a name='2.9.5'></a>
#### Giới hạn terminal và quản lý port

Tấn công DoS vào 1 thiết bị cơ sở hạ tầng có thể là mục tiêu là các cổng đầu cuối hoặc các cổng quản lý. Loại này dựa trên thực tế là dựa trên giới hạn số cổng đầu cuối và cổng quản lý có sẵn, và khi tất cả cổng đang được sử dụng ngay cả khi kết nối chưa được xác thực thì không thể thiết lập thêm kết nối. 

Các thiết bị phần mềm Cisco IOS  chỉ giới hạn các đường kết nối VTY, thường là 5 đường. Khi tất cả VTY được sử dụng, không có thêm kết nối từ xa nào được thiết lập nữa. Điều này tạo ra cơ hội cho một cuộc tấn công DoS nếu kẻ tấn công có thể mở sesion kết nối từ xa đến tất cả các VTYs có sẵn trên một hệ thống, ngăn không cho quản trị viên được truy cập. Attacker không cần phải đăng nhập để đạt được kiểu tấn công DoS này, các  sessions từ xa chỉ để lại ở dấu nhắc đăng nhập. Việc sử dụng AAA không làm giảm nhẹ kiểu tấn công này vì Attacker không cần phải đăng nhập, chỉ cần duy trì một kết nối đến cổng, do đó không cho phép người dùng khác sử dụng. 

Một cách để giải quyết kiểu tấn công này là thực hiện chính sách hạn chế truy cập mức độ cao trên một cổng đầu cuối hoặc cổng quản lý để bảo vệ sự sẵn có trong loại tấn công DoS này. Ví dụ, cổng này chỉ có thể được truy cập bởi một máy chủ NoC cụ thể.

Trong Cisco IOS, điều này có thể đạt được bằng cách cấu hình ACL với mức hạn chế cao trên VTY cuối cùng. VTY cuối cùng thường là VTY 4, có thể bị hạn chế chỉ chấp nhận các kết nối từ một trạm quản trị cụ thể duy nhất, chẳng hạn như máy chủ lưu trữ NoC được bảo vệ cao, trong khi các VTY khác chấp nhận kết nối từ bất kỳ địa chỉ nào trong dải địa chỉ rộng hơn, chẳng hạn như NoC .

```
!
access-list 10 permit <NOCsubnet> <inverse-mask>
access-list 20 permit host <NOC-Host>
line vty 0 3
access-class 10 in
line vty 4
access-class 20 in
!
```

<a name='2.10'></a>
### Bảng thông báo pháp lý

Nó khuyến cáo rằng thông báo cần được đưa ra trên tất cả các session tương tác để đảm bảo rằng người dùng được thông báo về chính sách bảo mật đang được thực thi và tuân thủ theo các tiêu chuẩn đó.

Trong một khu vực pháp lý, việc truy tố dân sự hoặc hình sự đối với attacker, người xâm nhập vào hệ thống, hoặc được yêu cầu nếu thông báo được đưa ra, thông báo cho những người sử dụng trái phép rằng việc sử dụng chúng thực tế là không được phép. Trong một số khu vực pháp lý, cũng có thể bị cấm giám sát hoạt động của người sử dụng trái phép trừ khi họ được thông báo được làm như vậy.

Các yêu cầu thông báo pháp lý rất phức tạp và thay đổi theo mỗi quyền hạn và vị trí. Ngay cả trong phạm vi quyền hạn, các ý kiến ​​pháp lý khác nhau và vấn đề này cần được thảo luận với luật sư riêng của bạn để đảm bảo rằng nó đáp ứng các yêu cầu pháp lý của công ty, địa phương và quốc tế. Điều này thường rất quan trọng để đảm bảo hành động thích hợp trong trường hợp vi phạm an ninh.

Trong hợp tác với các cố vấn của công ty, các tiêu chuẩn có thể được đưa vào thông báo pháp lý bao gồm:

- Thông báo rằng việc truy cập và sử dụng chỉ được cho phép người có quyền mới có thể xác thực, và có thông tin về ai có xác thực sử dụng.

- Thông báo việc truy cập và sử dụng hệ thống trái phép có thể bị sử phạt dân sự hoặc phạt hành chính.

- Thông báo rằng việc truy cập và sử dụng hệ thống có thể được đăng nhập hoặc giám sát mà không cần thông báo thêm, và các bản ghi kết quả có thể được dùng làm bằng chứng tại tòa án.

- Thêm các thông báo cụ thể được yêu cầu bởi luật tại địa phương cụ thể.

Từ một quan điểm an ninh, chứ không phải là quan điểm pháp lý, một biểu ngữ thông báo pháp luật không nên chứa bất kỳ thông tin cụ thể nào về thiết bị, như tên, mô hình, phần mềm, vị trí, nhà điều hành hoặc chủ sở hữu của nó bởi vì loại thông tin này có thể hữu ích Kẻ tấn công.

Một biểu mẫu pháp lý được cung cấp theo 1 chuẩn và sẽ được nói trong các phần sau

Trong Cisco IOS, một số biểu ngữ có sẵn bao gồm `banner motd`, `banner login`, `banner incoming`, và `banner exec`.

Khi một người sử dụng kết nối với một thiết bị IOS, một banner thông báo trong ngày (MOTD), nếu được cấu hình, sẽ xuất hiện đầu tiên, tiếp theo là một biểu ngữ đăng nhập (nếu được cấu hình) và một dấu nhắc đăng nhập. Sau khi người dùng đăng nhập thành công vào một thiết bị IOS, một biểu ngữ sẽ được hiển thị cho một đăng nhập Telnet ngược lại và một biểu ngữ EXEC sẽ được hiển thị cho tất cả các loại kết nối khác.

Khuyến cáo rằng chúng là banner MOTD hoặc đăng nhập để đảm bảo rằng một thông báo pháp luật được đưa ra trên tất cả các phiên truy cập quản lý thiết bị, trước khi dấu nhắc đăng nhập được đưa ra.

Thông tin nhiều hơn về lệnh `banner login` hoặc the `banner motd` xem tại http://www.cisco.com/en/US/docs/ios/12_3/configfun/command/reference/cfr_1g01.html#wp1029811 

<a name="3"></a>
## AAA Services

<a name="3.1"></a>
### AAA Overview

AAA là một khuôn khổ kiến ​​trúc để cấu hình một bộ ba chức năng bảo mật độc lập một cách nhất quán, mô đun hóa:

- Xác thực cho phép người dùng được xác định và xác minh trước khi họ được cấp quyền truy cập vào mạng và các dịch vụ mạng.

- Định nghĩa xác thực độ ưu tiên truy cập và hạn chế truy cập được thực thi đối với người dùng được xác thực.

- Cung cấp tài khoản có khả năng theo dõi truy cập của người dùng, bao gồm cả danh tính người dùng, thời gian bắt đầu và kết thúc truy cập, thực hiện các lệnh (CLI), số lượng các gói, và số byte.

AAA là phương thức căn bản và được khuyến khích để kiểm soát truy cập. Phần mềm Cisco IOS cung cấp các tính năng bổ sung cho kiểm soát truy cập đơn giản, như xác thực tên người dùng và xác thực mật khẩu các đường kết nối, tuy nhiên, các tính năng này không cung cấp mức độ kiểm soát truy cập tương tự với AAA và không được khuyến khích, ngay cả đối với các mô hình triển khai nhỏ. Ngay cả khi một máy chủ AAA riêng biệt không được triển khai, thì AAA tới cơ sở dữ liệu cục bộ nên được sử dụng trên thiết bị Cisco IOS.

Xác thực, ủy quyền và tài khoản AAA được thực thi bằng cách áp dụng các danh sách tên phương thức để truy cập vào các giao diện. Danh sách này sẽ được trình bày phần sau

AAA sử dụng các giao thức như RADIUS, TACACS+ hoặc Kerberos để quản lý các chức năng bảo mật của nó.

Thông tin chi tiết hơn về AAA service xem tại http://www.cisco.com/en/US/docs/ios/security/configuration/guide/sec_aaa_overview_ps6350_TSD_Products_Configuration_Guide_Chapter.html

#### Centralized AAA

Phương pháp được đề xuất để quản lý AAA nằm trên một máy chủ AAA tập trung với mật khẩu cục bộ như là một phương pháp dự phòng. Phương pháp dự phòng cục bộ cung cấp phương thức xác thực trong trường hợp không thể liên lạc với máy chủ AAA. Lợi ích chính của việc sử dụng máy chủ AAA tập trung bao gồm:

- __Manageability__ Username và password được lưu trữ riêng, vị trí trung tâm có thể được quản lý và sử dụng độc lập trên nhiều thiết bị.

- __Scalability__  Các máy chủ AAA có thể được mở rộng theo quy mô của cơ sở dữ liệu người dùng và số lần giao dịch mỗi giây.

- __Security__  Username và password trên toàn công ty có thể được lưu trữ bên ngoài router trong một hệ thống tập tin hoặc cơ sở dữ liệu mã hoá an toàn. Ngược lại, password lưu trữ cục bộ trên các thiết bị Cisco IOS, ngay cả khi được mã hóa, vẫn có thể bị reverse.

- __Accountability__  Các nỗ lực truy cập và session được ủy quyền có thể được đăng nhập độc lập trên máy chủ AAA

Nếu một máy chủ AAA tập trung hiện không được yêu cầu hoặc triển khai, vẫn nên thực hiện xác thực bằng cách sử dụng cấu hình AAA mặc dù cơ sở dữ liệu mật khẩu người dùng cục bộ sẽ được sử dụng. Điều này cho phép thực hiện các mật khẩu cục bộ của người dùng, thay vì tất cả người dùng sử dụng cùng mật khẩu đăng nhập hoặc mật khẩu. Cách tiếp cận này cung cấp khả năng bảo mật, khả năng hiển thị và kiểm soát tốt hơn, cùng với việc chuyển đổi dễ dàng hơn sang triển khai trong tương lai có thể tận dụng được một máy chủ AAA tập trung.

<a name="3.2"></a>
### AAA Server Groups

Trong Cisco IOS, một nhóm máy chủ AAA là danh sách các máy chủ lưu trữ AAA thuộc một loại cụ thể, ví dụ: RADIUS hoặc TACACS+, được sử dụng để thực hiện AAA. Nhóm máy chủ AAA cụ thể được sử dụng cho mỗi dịch vụ AAA cụ thể được xác định theo danh sách phương  thức AAA, như được thảo luận dưới đây.

Việc sử dụng tính năng nhóm máy chủ AAA (AAA server-group) cung cấp tính linh hoạt và kiểm soát tốt hơn so với các máy chủ AAA được sử dụng dù cho mục đích nào cũng như cung cấp dự phòng trên các máy chủ đã được định nghĩa.

Ví dụ, các máy chủ AAA khác nhau có thể được sử dụng cho các dịch vụ AAA khác nhau cho phép tách và ưu tiên quản lý truy cập thiết bị từ quản lý truy cập người dùng cuối thông qua việc sử dụng hai kho lưu trữ dữ liệu được duy trì và mở rộng độc lập. Ví dụ: quản lý truy cập thiết bị cơ sở hạ tầng có thể được xác thực bằng một bộ máy chủ TACACS+, trong khi truy cập mạng người dùng cuối có thể được xác thực bằng một bộ máy chủ RADIUS.

```
!
aaa group server tacacs+ adminAAAgroup
server TAC+server1
server TAC+server2
!
aaa group server radius enduserAAAgroup
server RADserver1
server RADserver2
!
```

Thông tin về `aaa group server` tại http://www.cisco.com/en/US/docs/ios/security/command/reference/sec_a1.html#wp1045019

<a name="3.3"></a>
### AAA Method Lists

Trong Cisco IOS, AAA được thực thi thông qua việc định nghĩa các danh sách các phương pháp có tên được áp dụng cho các giao diện truy cập. Danh sách phương pháp là danh sách tuần tự xác định phương thức xác thực hoặc ủy quyền được thi hành và trình tự chúng sẽ được thử. Danh sách các phương pháp cho phép một hoặc nhiều giao thức bảo mật được sử dụng để xác thực hoặc ủy quyền, đảm bảo tính sẵn có của một hệ thống backup dự phòng trong trường hợp phương pháp ban đầu không có sẵn.

Phần mềm Cisco IOS đầu tiên sẽ thử phương thức đầu tiên được liệt kê; Nếu phương thức đó không đáp ứng, phương thức kế tiếp trong danh sách các phương thức sẽ được cố gắng. Quá trình này tiếp tục cho đến khi có giao tiếp thành công với một phương thức được liệt kê hoặc danh sách các phương thức đã hết, trong trường hợp xác thực hoặc ủy quyền không thành công.

Một ví dụ được đặt tên theo danh sách các phương thức xác định với tên admin-list, có phương thức đầu tiên xác thực đến các máy chủ TACACAS + trong nhóm máy chủ adminAAAgroup, quay trở lại chứng thực cục bộ nếu các máy chủ đó không có sẵn như dưới đây:

```
aaa authentication login adminAuthen-list group adminAAAgroup local-case
aaa group server tacacs+ adminAAAgroup
server TAC+server1
server TAC+server2
```
Phần mềm Cisco IOS cố gắng xác thực hoặc ủy quyền bằng phương thức tiếp theo được liệt kê chỉ khi không có phản hồi từ phương thức trước đó. Nếu xác thực hoặc ủy quyền không thành công tại bất kỳ thời điểm nào trong chu kỳ này, có nghĩa là máy chủ bảo mật hoặc cơ sở dữ liệu tên người dùng cục bộ trả lời bằng cách từ chối truy cập của người dùng, quá trình dừng lại và không có các phương thức khác.

Một danh sách phương pháp AAA phải được áp dụng cho một đường truy cập trước khi nó được thi hành. Ngoại lệ duy nhất đối với trường hợp này là danh sách các phương pháp AAA mặc định (được đặt tên là `default`). Danh sách tên các phương  thức được tự động áp dụng cho tất cả các đường truy cập nếu không có danh sách phương pháp khác được áp dụng. Một danh sách phương pháp xác định sẽ ghi đè danh sách các phương pháp mặc định.

```
aaa authentication login default group enduserAAAgroup local-case
aaa group server radius enduserAAAgroup
server RADserver1
server RADserver2
```

Thông tin chi tiết về AAA method list và groups xem tại 

http://www.cisco.com/en/US/docs/ios/security/configuration/guide/sec_aaa_overview_ps6350_TSD_Products_Configuration_Guide_Chapter.html#wp1000933

http://www.cisco.com/en/US/docs/ios/security/configuration/guide/sec_cfg_radius_ps6350_TSD_Products_Configuration_Guide_Chapter.html#wp1001168

http://www.cisco.com/en/US/docs/ios/security/configuration/guide/sec_cfg_tacacs+_ps6350_TSD_Products_Configuration_Guide_Chapter.html#wp1001011

<a name="3.4"></a>
### AAA Server Communication Security

Truyền thông giữa một người dùng xác thực (như là NAS, Network Access Server) và một máy chủ AAA thường được thực hiện bằng RADIUS hoặc TACACS+. Sự an toàn của truyền thông này có thể được tóm tắt như sau:

- Cả hai giao thức RADIUS và TACACS+ đều được xác thực sử dụng chia sẽ, key bí mật kết hợp với tên thiết bị hoặc IP nhưng key bí mật này không bao giờ gửi qua mạng

- RADIUS, theo tiêu chuẩn thì chỉ mã hóa trường mật khẩu người dùng. Tất cả các gói dữ liệu khác đều thể hiện dưới dạng clear text và do đó dễ bị nghe lén

- TACACS+ mã hóa toàn bộ tải trọng của gói dữ liệu, do đó cung cấp một số dữ liệu bí mật, mặc dù thuật toán mã hóa không phải là rất mạnh.

Các hướng dẫn chung để bảo đảm truyền thông máy chủ AAA là:

- Sử dụng các key bí mật mạnh mẽ để xác thực máy chủ AAA và NAS

- Thường xuyên thay đổi các key bí mật được sử dụng để xác thực máy chủ AAA và NAS.

- Hạn chế truyền thông AAA giới hạn thiết lập xác thực máy chủ AAA, và qua cấu hình port truyền thông AAA thì nên sử dụng extended ACL.

Việc sử dụng ACL để hạn chế lưu lượng truy cập trực tiếp tới thiết bị cơ sở hạ tầng được trình bày chi tiết hơn trong các phần sau

- Vì RADIUS và TACACS+ không hỗ trợ chứng thực và mã hóa mạnh, nên khuyến cáo rằng một mạng out-of-band OOB hoặc IPSec management network được  xem như là một phương tiện để bảo vệ các máy chủ AAA khỏi bị tấn công.

<a name="3.5"></a>
### AAA Server Based Accounting Services

Điều quan trọng là phải đảm bảo quyền truy cập quản lý thiết bị được đăng nhập. Điều này được đề cập chi tiết hơn trong phần sau. Nhưng một phương thức đăng nhập quyền truy cập quản lý thiết bị là sử dụng tài khoản dựa trên máy chủ AAA

Tài khoản máy chủ AAA cho phép theo dõi các dịch vụ mà người dùng đang truy cập cũng như lượng tài nguyên mạng mà họ đang dùng. Khi tính năng account máy chủ AAA được kích hoạt, thiết bị cơ sở hạ tầng mạng báo cáo hoạt động của người dùng tới máy chủ AAA dưới dạng account record. Mỗi account record chứa các cặp attribute-value (AV) và được lưu trữ trên máy chủ AAA. Dữ liệu này sau đó có thể được phân tích để quản lý mạng, việc thanh toán của khách hàng hoặc kiểm toán.

Phần mềm Cisco hỗ trợ 5 loại account khác nhau

- __Network Accounting__ cung cấp thông tin các cả các session PPP, SLIP hoặc ARAP, bao gồm cả số packet và số byte

- __Connection Accounting__   cung cấp thông tin về tất cả các kết nối gửi đi được thực hiện từ máy chủ truy cập mạng, như Telnet, vận chuyển vùng cục bộ (LAT), TN3270, packet assembly-disassembly (PAD) và rlogin.

- __EXEC Accounting__  cung cấp thông tin về các thiết bị đầu cuối của người dùng EXEC trên máy chủ truy cập mạng, bao gồm tên người dùng, ngày, thời gian bắt đầu và kết thúc, địa chỉ IP của máy chủ truy cập và (cho người sử dụng quay số) số điện thoại

- __Command Accounting__  cung cấp thông tin về shell command EXEC cho một mức đặc quyền đang được thực thi trên một máy chủ truy cập mạng. Mỗi command accounting record bao gồm một danh sách các lệnh được thực hiện cho mức đặc quyền đó, cũng như ngày tháng và thời gian từng lệnh đã được thực hiện, và người dùng nào đã thực hiện nó.

- __System Accounting__   cung cấp thông tin về tất cả các sự kiện cấp hệ thống (ví dụ, khi hệ thống khởi động lại hoặc khi kế toán được bật hoặc tắt).

Cơ sở an ninh mạng tập trung vào việc đảm bảo cơ sở hạ tầng mạng và các dịch vụ mạng quan trọng. Do đó, account dựa trên AAA trong nền tảng này bao gồm:

- EXEC accounting

- Command accounting

- System accounting

<a name="4"></a>
## Secure Shell (SSH)

<a name="5"></a>
## Web-based GUI Access

<a name="5.1"></a>
### HTTP

<a name="5.2"></a>
### HTTPS

<a name="6"></a>
## SNMP Access

<a name="7"></a>
## Bảo vệ thông tin lưu trữ cục bộ

<a name="7.1"></a>
### Mã hõa mật khẩu toàn cục

<a name="7.2"></a>
### Mã hóa mật khẩu người dùng cục bộ

<a name="7.3"></a>
### Kích hoạt secret

<a name="8"></a>
## Chấp nhận truy cập quản lý thiết bị cơ sở hạ tầng 

<a name="8.1"></a>
### AAA EXEC Accounting

<a name="8.2"></a>
### AAA Failed Authentication Accounting

<a name="8.3"></a>
### AAA Command Accounting

<a name="8.4"></a>
### AAA System Accounting

<a name="8.5"></a>
### Syslog Login Success and Failure Notifications

<a name="8.6"></a>
### Configuration Change Notification and Logging

<a name="8.7"></a>
### Displaying Configuration Change Log Entries

<a name="8.8"></a>
### General Device Access and Configuration Change Logging Best Common Practices

<a name="9"></a>
## File Transfer

<a name="9.1"></a>
### File Transfer Protocol (FTP)

<a name="9.2"></a>
### Trivial File Transfer Program (TFTP)

<a name="9.3"></a>
### Secure Copy (SCP)

<a name="10"></a>
## Device Software Image Verification

<a name="10.1"></a>
### IOS Software Image Verification

<a name="11"></a>
## Infrastructure Management Network

<a name="12"></a>
## Device Management Best Common Practices


***
### Tham khảo

[1]. Network Security Baseline. http://www.cisco.com/c/en/us/td/docs/solutions/Enterprise/Security/Baseline_Security/securebasebook/sec_chap2.html

***