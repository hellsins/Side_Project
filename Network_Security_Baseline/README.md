Book: [Network Security Baseline](https://www.cisco.com/c/en/us/td/docs/solutions/Enterprise/Security/Baseline_Security/securebasebook.html)

#### Book Table of Contents

- [Introduction](https://gitlab.com/ctnguyenvn/Side_Project/-/tree/master/Network_Security_Baseline/Chapter01)

- [Infrastructure Device Access](https://gitlab.com/ctnguyenvn/Side_Project/-/tree/master/Network_Security_Baseline/Chapter02)

- [Routing Infrastructure](https://gitlab.com/ctnguyenvn/Side_Project/-/tree/master/Network_Security_Baseline/Chapter03)

- [Device Resiliency and Survivability](https://gitlab.com/ctnguyenvn/Side_Project/-/tree/master/Network_Security_Baseline/Chapter04)

- [Network Telemetry](https://gitlab.com/ctnguyenvn/Side_Project/-/tree/master/Network_Security_Baseline/Chapter05)

- [Network Policy Enforcement](https://gitlab.com/ctnguyenvn/Side_Project/-/tree/master/Network_Security_Baseline/Chapter06)

- [Switching Infrastructure](https://gitlab.com/ctnguyenvn/Side_Project/-/tree/master/Network_Security_Baseline/Chapter07)

- [Getting Started with Security Baseline](https://gitlab.com/ctnguyenvn/Side_Project/-/tree/master/Network_Security_Baseline/Chapter08)

- [Sample Configurations](https://gitlab.com/ctnguyenvn/Side_Project/-/tree/master/Network_Security_Baseline/Chapter09)

- [Commonly Used Protocols in the Infrastructure](https://gitlab.com/ctnguyenvn/Side_Project/-/tree/master/Network_Security_Baseline/Chapter10)
