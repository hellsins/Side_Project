
### Join Linux client vào Windows domain controller

> Thực hiện: Nguyễn Công Trứ

> Cập nhật: 03/05/2017

### Mục lục

[1. Cài đặt phần mềm](#1)

[2. Cấu hình cơ bản](#2)

- [2.1 Cấu hình IP address](#2.1)

- [2.2 Cấu hình time synchronization](#2.2)

- [2.3 Kiểm tra xác thực với kerberos](#2.3)

[3. Join domain sử dụng samba (winbind)](#3)

[4. Cấu hình xác thực các tài khoản trên máy linux](#4)

[5. Truy cập file chia sẻ của windows từ linux](#5)

- [5.1 Sử dụng nautilus-share](#5.1)

- [5.2 Sử dụng cifs](5.2)

***

<a name="1"></a>
### 1. Cài đặt phần mềm

Trước khi đi vào cài đặt ta tóm tắt 1 số thông tin như sau

|Windows Domain Controller|Linux Client|
|-----------|------------|
|Domain: dc.officesg.local|DNS: 10.0.10.10|
|IP: 10.0.10.10|IP: 10.0.10.20/24|
||Gateway: 10.0.10.1/24|


Xem thông tin cơ bản của máy Linux Client hiện tại

<p align='center'> <img src="https://gitlab.com/hellsins/Side_Project/raw/master/EndPoint_Security/Image/1.png"/></p>

> Lưu ý: Chúng ta có thể thay đổi hostname của máy tính với lệnh `sudo hostnamectl set-hostname [computer_name]`


Để thuận tiện trong quá trình cài đặt và cấu hình về sau, tôi sẽ cài đặt tất cả các phần mềm cần dùng trong bước này như sau

	sudo apt-get install ntp ntpdate samba krb5-config krb5-user winbind libpam-winbind libnss-winbind -y

Trong quá trình cài đặt đến kerberos sẽ xuất hiện thông báo nhập domain name server. Chúng ta điền như sau

<p align='center'> <img src="https://gitlab.com/hellsins/Side_Project/raw/master/EndPoint_Security/Image/3.png"/></p>

<p align='center'> <img src="https://gitlab.com/hellsins/Side_Project/raw/master/EndPoint_Security/Image/4.png"/></p>

<p align='center'> <img src="https://gitlab.com/hellsins/Side_Project/raw/master/EndPoint_Security/Image/5.png"/></p>

> Lưu ý: Chúng ta có thể edit thông tin này sau khi cài đặt ở file `/etc/krb5.conf`

<a name="2"></a>
### 2. Cấu hình cơ bản

<a name="2.1"></a>
#### 2.1 Cấu hình IP address

Để cấu hình IP cho ubuntu ta edit file `/etc/network/interfaces`. Và trỏ nameserver về máy DC ở file `/etc/resolv.conf`

<p align='center'> <img src="https://gitlab.com/hellsins/Side_Project/raw/master/EndPoint_Security/Image/2.png"/></p>

Khởi động lại network với lệnh

	sudo systemctl restart networking.service

Lưu ý vì ở đây tôi dùng trên vmware nên cần chỉnh sửa file `/etc/hosts` để connect tới máy DC (ở cùng network)

<p align='center'> <img src="https://gitlab.com/hellsins/Side_Project/raw/master/EndPoint_Security/Image/6.png"/></p>

<a name="2.2"></a>
#### 2.2 Cấu hình time synchronization

Để join linux client vào DC ta cần đồng bộ thời gian cho chúng. Việc đồng  bộ thời gian rất quan trọng vì nếu không sẽ gặp lỗi trong quá trình join (sẽ nói sau)

Chúng ta có thể edit file `/etc/ntp.conf` hoặc sử dụng các lệnh sau để update  time synchronization với ntpdate đã được cài đặt phía trên

<p align='center'> <img src="https://gitlab.com/hellsins/Side_Project/raw/master/EndPoint_Security/Image/7.png"/></p>

> Lưu ý: Thời gian chênh lệch tối đa giữa 2 máy là 5 phút

<a name="2.3"></a>
#### 2.3 Kiểm tra xác thực với kerberos

Tiếp theo ta cần kiểm tra 2 máy đã connect được hay chưa 

	ping -c4 dc.officesg.local

Và dùng kerberos để xác thực với tài khoản AD Administrator

<p align='center'> <img src="https://gitlab.com/hellsins/Side_Project/raw/master/EndPoint_Security/Image/8.png"/></p>

<a name="3"></a>
### 3. Join domain sử dụng samba (winbind)

Bước đầu tiên ta cần chỉnh sửa file cấu hình tại `/etc/samba/smb.conf`

<p align='center'> <img src="https://gitlab.com/hellsins/Side_Project/raw/master/EndPoint_Security/Image/9.png"/></p>

Trong đó: 

|Tham số|Ý nghĩa|
|------|-------|
|workgroup| Tên workgroup (domain) của DC|
|realm|Domain name server|
|netbios name|Tên hostname của máy Linux client|
|security||
|dns forwarder|DNS của DC|
|idmap|Chỉ ID của user khi map qua bên linux. Ví dụ ở đây user đầu tiên khi map qua sẽ có ID là 5001|
|template homedir|Chỉ home của user khi login lần đầu|
|template shell |Chỉ shell của user (bên windows) sử dụng|
|winbind use default domain||
|winbind nss info||
|winbind enum||

Tiếp theo ta loại bỏ các chương trình không cần thiết, khởi động lại samba và enable samba

<p align='center'> <img src="https://gitlab.com/hellsins/Side_Project/raw/master/EndPoint_Security/Image/10.png"/></p>

Cuối cùng để join máy Linux client vào windows domain (sử dụng tài khoản AD administrator) ta làm như sau

<p align='center'> <img src="https://gitlab.com/hellsins/Side_Project/raw/master/EndPoint_Security/Image/11.png"/></p>

> Lưu ý: Ở bước này có thể xảy ra rất nhiều lỗi khác nhau, ví dụ như phía trên ta chưa update được `time synchronization` sẽ phát sinh lỗi `gss_init_sec_context failed with: Miscellaneous failure: Clock skew too great`. Để xem một số lỗi phổ biến ta xem tại [đây](https://wiki.samba.org/index.php/Troubleshooting_Samba_Domain_Members)

Chúng ta có thể kiểm tra bên máy DC quá trình join domain đã thành công bằng cách trên windows DC chọn  `Start -> Administrator Tools -> Active Directory Users and Computers -> Builtin -> Computers`

<p align='center'> <img src="https://gitlab.com/hellsins/Side_Project/raw/master/EndPoint_Security/Image/12.png"/></p>

<a name="4"></a>
### 4. Cấu hình xác thực các tài khoản trên máy linux

Để xác thực cho các tài khoản AD trên máy local (Linux Client), ta cần sửa đổi một số tập tin trên máy local

Đầu tiên chỉnh sửa file `/etc/nsswitch.conf` 

<p align='center'> <img src="https://gitlab.com/hellsins/Side_Project/raw/master/EndPoint_Security/Image/13.png"/></p>

Tiếp theo ta chỉnh sửa 1 số dòng ở file `/etc/krb5.conf` như hình 

<p align='center'> <img src="https://gitlab.com/hellsins/Side_Project/raw/master/EndPoint_Security/Image/14.png"/></p>

<p align='center'> <img src="https://gitlab.com/hellsins/Side_Project/raw/master/EndPoint_Security/Image/15.png"/></p>

<p align='center'> <img src="https://gitlab.com/hellsins/Side_Project/raw/master/EndPoint_Security/Image/16.png"/></p>

Tiếp tục ta cần khởi động lại samba, winbind và kiểm tra các users, groups đã được map qua máy local hay chưa

<p align='center'> <img src="https://gitlab.com/hellsins/Side_Project/raw/master/EndPoint_Security/Image/17.png"/></p>

Nếu thành công (xuất hiện các users/groups bên DC) thì ta có thể xem các thông tin id, home, shell... của users/groups như sau

<p align='center'> <img src="https://gitlab.com/hellsins/Side_Project/raw/master/EndPoint_Security/Image/18.png"/></p>

Tiếp theo để update và thiết lập các thông số xác thực cũng như chạy các dịch vụ winbind, tạo home directory trong lần login đầu tiên,... ta cập nhật như sau

	sudo pam-auth-update

<p align='center'> <img src="https://gitlab.com/hellsins/Side_Project/raw/master/EndPoint_Security/Image/19.png"/></p>

Tiếp theo ta chỉnh sửa file `/etc/pam.d/common-account` để tạo thư mục home cho xác thực domain users

<p align='center'> <img src="https://gitlab.com/hellsins/Side_Project/raw/master/EndPoint_Security/Image/20.png"/></p>

Tiếp theo ta chỉnh sửa file `/etc/pam.d/common-password` như sau để cho phép users bên DC có thể thay đổi password từ giao diện dòng lệnh

<p align='center'> <img src="https://gitlab.com/hellsins/Side_Project/raw/master/EndPoint_Security/Image/21.png"/></p>

Bây giờ ta có thể logout và login lại để xem kết quả hoặc có thể dùng `su` để chuyển sang các user AD khác để kiểm tra

<p align='center'> <img src="https://gitlab.com/hellsins/Side_Project/raw/master/EndPoint_Security/Image/22.png"/></p>

Cuối cùng, trên 1 số bản phân phối linux, ở đây là ubuntu có thể sẽ tắt chế độ login với root ta có mở lên bằng cách chỉnh sửa file `/usr/share/lightdm/lightdm.conf.d/50-ubuntu.conf`

<p align='center'> <img src="https://gitlab.com/hellsins/Side_Project/raw/master/EndPoint_Security/Image/23.png"/></p>

<a name="5"></a>
### 5. Truy cập file chia sẻ của windows từ linux

<a name="5.1"></a>
#### 5.1 Sử dụng nautilus-share

`Nautilus-share` là chương trình cho phép truy cập files chia sẻ từ giao diện đồ họa rất đơn giản. Tuy nhiên chỉ làm việc ở session hiện tại, khi logout thì sẽ mất (không tự động mount khi login lại)

Đối với các bản phân phối `nautilus-share` hầu hết được cài đặt. Tuy nhiên nếu chưa ta có thể cài (trên ubuntu) với 

	sudo apt-get install nautilus-share

Tiếp theo để connect tới file/folder chia sẻ từ windows DC ta làm như sau

<p align='center'> <img src="https://gitlab.com/hellsins/Side_Project/raw/master/EndPoint_Security/Image/24.png"/></p>

> Lưu ý:

> - 10.0.10.10 là IP máy chia sẻ tài nguyên

> - ShareFolder là thư mục mà phía DC chia sẻ (cần thay đổi đường dẫn phù hợp)

> - Một số bản phân phối không có ô bước thứ 2 như xfce ta có thể khởi động `nautilus` với lệnh `nautilus-connect-server` và thực hiện connect như trên

Sau khi connect xong ta sẽ thấy thư mục chia sẻ như sau

<p align='center'> <img src="https://gitlab.com/hellsins/Side_Project/raw/master/EndPoint_Security/Image/25.png"/></p>

<a name="5.2"></a>
#### 5.2 Sử dụng cifs

Đối với môi trường dòng lệnh có rất nhiều tùy chọn như `smbclient`, `smbfs`, tuy nhiên ở đây tôi sử dụng `cifs`. Đây tuy là phiên bản cũ nhưng vẫn được hỗ trợ trên các bản phân phối mới như ubuntu 16.04. Ngược lại `smbfs` đã không còn hỗ trợ. Thêm 1 ưu điểm nữa là chúng ta có thể cấu hình auto mount các tài nguyên được share từ windows DC. 

Chúng ta cần cài `cifs-utils` và cấu hình chúng như sau:

Chúng ta cần thêm quyền mount các thiết bị cho user thuộc group cần thêm bằng cách thêm dòng sau vào file `/etc/sudoer`

<p align='center'> <img src="https://gitlab.com/hellsins/Side_Project/raw/master/EndPoint_Security/Image/27.png"/></p>

> Lưu ý: `%domain\ users` là group cần thêm quyền sử dụng các lệnh phía sau

Tiếp theo ta tạo file để chứa tài nguyên được chia sẻ từ server. Ở đây tôi tạo tại home của user hiện tại

	mkdir ~/ShareFolder

Cuối cùng ta dùng lệnh sau để connect (mount) đến file/folder của windows DC

<p align='center'> <img src="https://gitlab.com/hellsins/Side_Project/raw/master/EndPoint_Security/Image/28.png"/></p>

> Lưu ý: 

> - `username` và `password` là tài khoản của user chia sẻ data bên windows DC.

> - `uid` và `gid` là thông tin của user hiện tại. Dùng lệnh `id` để xem chi tiết

> - `//10.0.10.10/ShareFolder` là IP và folder của windows DC

> - `ShareFolder` là thư mục hiện tại trên máy Linux client. Lưu ý nếu thay đổi vị trí nó thì cần thay đổi đường dẫn phù hợp

> - Nếu không chỉ định `uid` và `gid` thì thư mục sau khi mount sẽ tự động chuyển sang quyền `root:root` vì ta đang sử dụng `sudo`

Nếu muốn umount ta dùng lệnh

	sudo umount [thư_mục_cần_umount]

##### Bây giờ ta cần cấu hình để mount tự động khi khởi động máy. Để làm điều đó ta cần:

Đầu tiên ta tạo file `/etc/samba/user` với nội dung sau

```
username=admin
password=Password
```

Trong đó:

- `admin` và `password` là tài khoản của user share file trên server (ở đây là windows DC)

Tiếp theo ta phân quyền cho file này

	sudo chmod 400 /etc/samba/user

Tiếp theo ta cần edit file `/etc/fstab` để quá trình mount tự động được diễn ra khi khởi động

<p align='center'> <img src="https://gitlab.com/hellsins/Side_Project/raw/master/EndPoint_Security/Image/26.png"/></p>

Tương tự như trên nếu ta không chỉ định `uid` và `gid` (lưu ý cần `nounix`) thì khi khởi động mặc định permision sẽ  là `root:root`. 

***

### Tham khảo

[1]. Matei Cezar. (2017). Integrate Ubuntu 16.04 to AD as a Domain Member with Samba and Winbind – Part 8. http://www.tecmint.com/join-ubuntu-to-active-directory-domain-member-samba-winbind/

[2]. Troubleshooting Samba Domain Members. https://wiki.samba.org/index.php/Troubleshooting_Samba_Domain_Members

[3]. Mark Heslin. Integrating Red Hat Enterprise Linux 6 with Active Directory. http://docplayer.net/3591694-Integrating-red-hat-enterprise-linux-6-with-active-directory-mark-heslin-principal-software-engineer.html

[4]. Samba/SambaClientGuide. https://help.ubuntu.com/community/Samba/SambaClientGuide

***